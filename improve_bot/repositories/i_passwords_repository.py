from abc import ABC, abstractmethod

from improve_bot.entities.passwords import Password

class IPasswordRepository(ABC):

	@abstractmethod
	def add(self, password: Password):

		pass

	@abstractmethod
	def get_all(self):

		pass

	@abstractmethod
	def delete(self, uid: int):

		pass