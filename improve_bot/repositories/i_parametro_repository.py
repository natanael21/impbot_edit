from abc import ABC, abstractmethod

from improve_bot.entities.parametros import Parametro

class IParametroRepository(ABC):

	@abstractmethod
	def add(self, api: Parametro):

		pass

	@abstractmethod
	def get_all(self):

		pass
	
	@abstractmethod
	def get_parametro_by_id(self, id: int):

		pass

	@abstractmethod
	def delete(self):

		pass