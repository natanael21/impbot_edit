from abc import ABC, abstractmethod

from improve-bot.entities.users import User


class IUserRepository(ABC):

	@abstractmethod
	def add(self, user: User):

		pass

	@abstractmethod
	def get_all(self):

		pass

	@abstractmethod
	def get_user_by_username(self, user_name: str):

		pass

	@abstractmethod
	def get_user_by_id(self, id: int):

		pass

	@abstractmethod
	def delete(self, uid: int):

		 pass