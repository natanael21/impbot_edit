from abc import ABC, abstractmethod

from improve_bot.entities.sessions import Session

class ISessionRepository(ABC):

	@abstractmethod
	def add(self, session: Session):

		pass

	@abstractmethod
	def get_all(self):

		pass

	@abstractmethod
	def delete(self, uid: int):

		pass