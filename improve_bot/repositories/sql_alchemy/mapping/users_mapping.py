from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String
from sqlalchemy.orm import mapper, relationship

def Users_Ibot_mapping(metadata: MetaData):

    Users_Ibot = Table(
        'tbl_ibot_users',
        metadata,
        Column('id', Integer, Sequence('Users_id_seq'), primary_key=True, nullable=False),
        Column('idreg_fk_perfil', Integer, ForeignKey('tbl_ibot_perfil.id'), nullable=False),
        Column('idreg_fk_customer', Integer, ForeignKey('tbl_ibot_customer.id'), nullable=False),
        Column('user_name', String(20), nullable=False),
        Column('name', String(50), nullable=False),
        Column('mood', String(50)),
        Column('mobile', String(50)),
        Column('home', String(50)),
        Column('office', String(50)),
        Column('about_me', String(255)),
        Column('country_region', String(50)),
        Column('city', String(50)),
        Column('comuna', String(50)),
        Column('website', String(50)),
        Column('gender', String(1)),
        Column('birthdate', DateTime),
        Column('password', String(15), nullable=False),
        Column('language', String(50)),
        Column('password_v2', String(15)),
        Column('first_time', String(1), nullable=False),
        Column('tutorial', String(1), nullable=False),
        Column('account_sid', String(20)),
        Column('auth_token', String(255), nullable=False),
        Column('ghost_user', String(1)),
        Column('empres', String(25), nullable=False),
        Column('photo', String(100), nullable=False),
        Column('licencia', String(50), nullable=False),
        Column('user_mail', String(50), nullable=False),
        Column('user_mail_gmail', String(50)),
        Column('consentimiento', String(1),nullable=False),
        Column('fecha_con', DateTime),
        Column('active', String(1), nullable=False),
        Column('audit_fec_first_time', DateTime, nullable=False),
        Column('audit_log_ins', Integer, nullable=False),
        Column('audit_fec_ins', DateTime, nullable=False),
        Column('audit_log_udp', Integer, nullable=False),
        Column('audit_fec_udp', DateTime, nullable=False),
        info={'bind_key': 'ibot'}        
    )
    mapper(User, user, properties={
		'tbl_ibot_perfil':relationship(Perfil,backref='user', order_by=user.c.id),
		'tbl_ibot_customer':relationship(Customer,backref='user', order_by=user.c.id)
	})

    return Users_Ibot