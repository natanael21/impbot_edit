import datetime

from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String
from sqlalchemy.orm import mapper, relationship

from improve_bot.entities.sessions import Session

def session_mapping(metadata: MetaData):
	session = Table(
		'tbl_ibot_session',
		metadata,
		Column('id', Integer, Sequence('sesiones_id_seq'), primary_key=True, nullable=False),
		Column('user_name', String(20), nullable=False),
		Column('session_ip_address', String(15), nullable=False),
		Column('session_remote_host', String(15), nullable=False),
		Column('session_user_agent', String(255), nullable=False),
		Column('session_start_time', DateTime, nullable=False),
		Column('session_end_time', DateTime, nullable=True),
		Column('session_time', Integer, nullable=True),
		Column('session_show_popup', String(1), nullable=True),
		Column('active', String(1), nullable=False),
		Column('audit_fec_ins', DateTime, default=datetime.datetime.now, nullable=False),
		info={'bind_key': 'ibot'}
	)
	mapper(Session, session)

	return session