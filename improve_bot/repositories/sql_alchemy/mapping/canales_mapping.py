from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String
from sqlalchemy.orm import mapper, relationship

from improve_bot.entities.canales import Canales

def canales_mapping(metadata: MetaData):
    canales= Table(
        'tbl_ibot_canales',
        metadata,
        Column('id',Integer, Sequence('canales_id_seq'), primary_key=True, nullable=False),
        Column('nombre_canal', String(25), nullable=False),
        Column('desc_canal', String(255), nullable=False),
        Column('active', String(1), nullable=False),
		Column('audit_log_ins', Integer, nullable=False),
		Column('audit_fec_ins', DateTime, nullable=False),
		Column('audit_log_upd', Integer, nullable=False),
		Column('audit_fec_upd', DateTime, nullable=False),
		info={'bind_key': 'ibot'}
    )

    mapper(Canales, canales)

    return canales