from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String
from sqlalchemy.orm import mapper, relationship

from improve_bot.entities.nub_usu_can_api import Nub_usu_can_api

def Nub_usu_can_api_mapping(metadata: MetaData):

    nub_usu_can_api = Table(
        'tbl_ibot_nub_can_api',
        metadata,
        Column('id', Integer, Sequence('nub_can_api_id_seq'), primary_key= True, nullable=False),
        Column('idreg_fk_usuario', Integer, ForeignKey('tbl_ibot_users.id'), nullable=False ),
    )