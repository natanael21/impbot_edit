from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String
from sqlalchemy.orm import mapper, relationship

from improve_bot.entities.perfil import Perfil

def perfil_mapping(metadata: MetaData):
	perfil = Table('tbl_kwa_perfil',
		metadata,
		Column('id', Integer, Sequence('perfiles_id_seq'), primary_key=True, nullable=False),
		Column('name_perfil', String(50), nullable=False),
		Column('desc_perfil', String(255), nullable=False),
		Column('active', String(1), nullable=False),
		Column('audit_log_ins', Integer, nullable=False),
		Column('audit_fec_ins', DateTime, nullable=False),
		Column('audit_log_upd', Integer, nullable=False),
		Column('audit_fec_upd', DateTime, nullable=False),
		info={'bind_key': 'ibot'}
	)
	mapper(Perfil, perfil)

	return perfil