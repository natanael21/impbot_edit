import datetime

from sqlalchemy import Table, MetaData, Column, Integer, String, Sequence, DateTime, Text
from sqlalchemy.orm import mapper, relationship

from improve_bot.entities.parametros import Parametro

def parameters_mapping(metadata: MetaData):
	parametro = Table(
			'tbl_ibot_parameters',
			metadata,
			Column('id', Integer, Sequence('application_id_seq'), primary_key=True, nullable=False),
			Column('parameter_name', String(50)),
			Column('desc_parameter', String(254)),
			Column('value_parameter', Text),
			Column('active', String(1), nullable=False),
			Column('audit_log_ins', Integer, nullable=False),
			Column('audit_fec_ins', DateTime, default=datetime.datetime.now, nullable=False),
			Column('audit_log_upd', Integer, nullable=False),
			Column('audit_fec_upd', DateTime, default=datetime.datetime.now, nullable=False),
				info={'bind_key': 'ibot'}
	)

	mapper(Parametro, parametro)

	return parametro