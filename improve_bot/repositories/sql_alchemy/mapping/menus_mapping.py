from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String
from sqlalchemy.orm import mapper, relationship

from improve_bot.entities.menu import Menu

def menu_mapping(metadata: MetaData):
	menu = Table(
		'tbl_ibot_menus',
		metadata,
		Column('id', Integer, Sequence('canales_id_seq'), primary_key=True, nullable=False),
		Column('name_menu', String(50), nullable=False),
		Column('raiz', Integer, nullable=False),
		Column('link', String(255), nullable=False),
		Column('active', String(1), nullable=False),
		Column('audit_log_ins', Integer, nullable=False),
		Column('audit_fec_ins', DateTime, nullable=False),
		Column('audit_log_upd', Integer, nullable=False),
		Column('audit_fec_upd', DateTime, nullable=False),
		info={'bind_key': 'ibot'}
	)

	mapper(Menu, menu)

	return menu