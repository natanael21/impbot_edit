from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String, Binary
from sqlalchemy.orm import mapper, relationship

from improve_bot.entities.passwords import Password

def password_mapping(metadata: MetaData):
	password = Table(
		'tbl_ibot_password',
		metadata,
		Column('id', Integer, Sequence('passwords_id_seq'), primary_key=True, nullable=False),
		Column('core_security_pass_userid', Integer, unique=True, nullable=True),
		Column('core_security_pass_hist_1', Binary(60), nullable=True),
		Column('core_security_pass_hist_2', Binary(60), nullable=True),
		Column('core_security_pass_hist_3', Binary(60), nullable=True),
		Column('core_security_pass_last', Binary(60), nullable=True),
		Column('core_security_pass_op', Integer, nullable=True),
		Column('core_security_pass_block', Integer, nullable=True),
		Column('core_security_pass_ind', Integer, nullable=True),
		Column('core_security_date_block', DateTime, nullable=True),
		Column('core_security_pass_load_type', String(25), nullable=True),
		Column('audit_log_ins', Integer, nullable=False),
		Column('audit_fec_ins', DateTime, nullable=False),
		Column('audit_log_upd', Integer, nullable=False),
		Column('audit_fec_upd', DateTime, nullable=False),
		info={'bind_key': 'kwa'}
	)
	mapper(Password, password)
	
	return password