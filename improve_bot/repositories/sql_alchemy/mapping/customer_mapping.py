from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String
from sqlalchemy.orm import mapper, relationship

from improve_bot.entities.customers import Customer

def customer_mapping(metadata: MetaData):
	customer = Table(
		'tbl_ibot_customers',
		metadata,
		Column('id', Integer, Sequence('customers_id_seq'), primary_key=True, nullable=False),
        Column('idreg_fk_plan', Integer, ForeignKey('tbl_ibot_planes.id')),
		Column('rut_customer', String(20), nullable=False),
		Column('name_customer', String(20),unique=True, nullable=False),
		Column('desc_customer', String(255), nullable=False),
		Column('address_customer', String(255), nullable=False),
		Column('phone_customer', String(255), nullable=False),
		Column('rubro_customer', String(255), nullable=False),
		Column('nro_workers', Integer, nullable=False),
		Column('email_customer', String(50), nullable=False),
		Column('logo_customer', String(50), nullable=False),
		Column('active', String(1), nullable=False),
		Column('audit_log_ins', Integer, nullable=False),
		Column('audit_fec_ins', DateTime, nullable=False),
		Column('audit_log_upd', Integer, nullable=False),
		Column('audit_fec_upd', DateTime, nullable=False),
		info={'bind_key': 'ibot'}
	)

	mapper(Customer, customer)

	return customer