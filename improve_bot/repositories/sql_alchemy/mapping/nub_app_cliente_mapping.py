from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String
from sqlalchemy.orm import mapper, relationship

from improve_bot.entities.nub_app_cliente import Nub_App_Cliente

def Nub_App_Cliente_mapping(metadata: MetaData):
    nub_app_cliente = Table('tbl_ibot_nub_app_cliente'
                    metadata,
                    Column('id', Integer, Sequence('nub_app_cliente_id_seq'), primary_key= True, nullable=False),
                    Column('idreg_fk_app', Integer, ForeignKey('tbl_ibot_application.id'), nullable=False),
                    Column('idreg_fk_cliente', Integer, ForeignKey('tbl_ibot_users.id'), nullable=False),
                    Column('active', String(1), nullable=False),
		            Column('audit_log_ins', Integer, nullable=False),
		            Column('audit_fec_ins', DateTime, nullable=False),
		            Column('audit_log_upd', Integer, nullable=False),
		            Column('audit_fec_upd', DateTime, nullable=False),
		            info={'bind_key': 'ibot'}
        )
    mapper(Nub_App_Cliente, nub_app_cliente)

    return nub_App_Cliente
            