from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String
from sqlalchemy.orm import mapper, relationship

from improve_bot.entities.application import Application

def application_mapping(metadata: MetaData):
    applciation = Table(
        'tbl_ibot_application',
        metadata,
        Column('id', Integer, Sequence('application_id_seq'), primary_key=True, nullable=False),
        Column('name_application', String(70), nullable=False),
        Column('desc_application', String(255), nullable=False),
        Column('url_app', String(255)),
        Column('icon_app', String(50)),
        Column('active', String(1), nullable=False),
        Column('audit_log_ins', Integer, nullable=False),
		Column('audit_fec_ins', DateTime, nullable=False),
		Column('audit_log_upd', Integer, nullable=False),
		Column('audit_fec_upd', DateTime, nullable=False),
        info={'bind_key': 'ibot'}
    )

    mapper(Application, applciation)

    return applciation