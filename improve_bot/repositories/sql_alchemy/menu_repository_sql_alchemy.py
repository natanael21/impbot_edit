from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from improve_bot.entities.menu import Menu

from improve_bot.repositories.i_menu_repository import IMenuRepository

class MenuRepositorySqlAlchemy(IMenuRepository):

	@inject
	def __init__(self, db: SQLAlchemy):

		self.db = db

	def add(self, menu: Menu):

		try:
			self.db.session.add(menu)
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()

	def get_all(self):

		return self.db.session.query(Menu).filter(Menu.active == 's').order_by(Menu.name_menu).all()

	def get_id(self, mid: int):

		menu = self.db.session.query(Menu).get(mid)

	def get_by_name(self, name:str):

		menu = self.db.session.query(Menu).filter(Menu.name_menu == name).first()

		if menu:
			return menu

	def update(self):

		self.db.session.commit()

	def delete(self, mid: int):

		try:
			self.db.session.query(Menu).filter(Menu.id == mid).delete()
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()