from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from sqlalchemy import cast, Date

from improve_bot.entities.canales import Canales
from improve_bot.repositories.i_canales_repository import ICanalesRepository

class CanalesRepositorySqlAlchemy(ICanalesRepository):
    