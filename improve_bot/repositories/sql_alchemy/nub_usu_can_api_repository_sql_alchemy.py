from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from improve_bot.entities.nub_usu_can_api import Nub_usu_can_api

from improve_bot.repositories.i_nub_usu_can_api import INub_usu_can_api

class NubUsuCanApiRepositorySqlAlchemy(INub_usu_can_api):