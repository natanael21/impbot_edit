from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from sqlalchemy import cast, Date

from improve_bot.entities.nub_app_cliente import Nub_App_Cliente
from improve_bot.repositories.i_nub_app_cliente_repositoy import INubAppClienteRepository

class NubAppClienteRepositorySqlAlchemy(INubAppClienteRepository):
    