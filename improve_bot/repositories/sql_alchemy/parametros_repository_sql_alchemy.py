from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from improve_bot.entities.parametros import Parametro

from improve_bot.repositories.i_parametro_repository import IParametroRepository

class ParametroRepositorySqlAlchemy(IParametroRepository):  

	@inject
	def __init__(self, db: SQLAlchemy):

		self.db = db

	def add(self, parametro: Parametro):

		try:
			self.db.session.add(parametro)
			self.db.session.commit()

		except IntegrityError:
			self.db.session.rollback()
			raise InvalidUserException    

	def get_all(self):

		return self.db.session.query(Parametro).filter(Parametro.id > 0).all()    

	def get_parametro_by_id(self, id: int):

		parametro = self.db.session.query(Parametro).filter(Parametro.id == id).first()
		if parametro:
			return parametro
		raise InvalidApiException

	def get_parametro_by_name(self, name: str):

		parametro = self.db.session.query(Parametro).filter(Parametro.parameter_name == name
															).first()
		if parametro:
			return parametro.value_parameter
		else:
			return 'undefined'

	def update(self):

		self.db.session.commit()

	def delete(self, uid: int):

		try:
			self.db.session.query(Parametro).filter(Parametro.id == uid).delete()
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()
			raise InvalidApiException