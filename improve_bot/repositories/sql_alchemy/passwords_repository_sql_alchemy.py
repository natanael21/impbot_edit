from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from improve_bot.entities.users import User
from improve_bot.entities.passwords import Password

from improve_bot.repositories.i_passwords_repository import IPasswordRepository

class PasswordRepositorySqlAlchemy(IPasswordRepository):

	@inject
	def __init__(self, db: SQLAlchemy):
		self.db = db

	def add(self, password: Password):

		try:
			self.db.session.add(password)
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()
			raise InvalidUserException

	def get_all(self):

		return self.db.session.query(Password).all()

	def get_pass(self, user_id: str):

		oPass = self.db.session.query(Password).filter(Password.core_security_pass_userid == str(user_id)
													  ).first()
		if oPass:
			return oPass
		raise InvalidUserException

	def update_pass_block_by_userid(self, user_id: str, core_security_pass_block: int):

		oPass = self.db.session.query(Password).filter(Password.core_security_pass_userid == str(user_id)
													  ).first()
		if oPass:
			oPass.core_security_pass_block = core_security_pass_block
			self.db.session.commit()

	def update_pass_ind_by_userid(self, user_id: str, core_security_pass_ind: int):

		oPass = self.db.session.query(Password).filter(Password.core_security_pass_userid == str(user_id)
													  ).first()
		if oPass:
			oPass.core_security_pass_ind = core_security_pass_ind
			self.db.session.commit()

	def update(self):

		self.db.session.commit()

	def delete(self, uid: int):

		try:
			self.db.session.query(Password).filter(Password.id == uid).delete()
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()
			raise InvalidUserException