from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from sqlalchemy import cast, Date

from improve_bot.entities.customers import Customer

from improve_bot.repositories.i_customer_repository import ICustomerRepository

class CustomerRepositorySqlAlchemy(ICustomerRepository):

	@inject
	def __init__(self, db: SQLAlchemy):

		self.db = db

	def add(self, customer: Customer):

		try:
			self.db.session.add(customer)
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()

	def get_all(self):

		query = self.db.session.query(Customer).order_by(Customer.id).all()
		return query

	def get_customer_by_id(self, cid: int):

		customer = self.db.session.query(Customer).filter(Customer.id == cid).first()
		if customer:
			return customer
		raise InvalidApiException

	def get_by_name(self, name: str):

		customer = self.db.session.query(Customer).filter(Customer.name_customer == name).first()
		if customer:
			return customer
		raise InvalidApiException

	def get_all_without_active(self):

		return self.db.session.query(Customer).order_by(Customer.id).all()

	
	def update(self):

		self.db.session.commit()

	def update(self):

		self.db.session.commit()

	def delete(self, cid: int):

		try:
			self.db.session.query(Customer).filter(Customer.id == cid).delete()
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()