from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from improve-bot.entities.users import User
from improve-bot.entities.perfil import Perfil

from saas_Krino.repositories.i_users_repository import IUserRepository

class UserRepositorySqlAlchemy(IUserRepository):

    @inject
    def __init__(self, db: SQLAlchemy):

		self.db = db
    
    def add(self, user: User):

		try:
			self.db.session.add(user)
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()
			raise InvalidUserException

    def get_all(self):

		return self.db.session.query(User).all()

    def get_user_by_username(self, user_name: str):

		user = self.db.session.query(User).filter(User.user_name == user_name).first()
		if user:
			return user

    def get_user_by_id(self, id: int):

		user = self.db.session.query(User).filter(User.id == id).first()
		if user:
			return user
			
		raise InvalidUserException

    def update(self):

		self.db.session.commit()

    def update_first_time(self, user_name):

		user = self.db.session.query(User).filter(User.user_name == user_name).first()
		if user:
			user.first_time = 's'
			self.db.session.commit()

    def get_users_by_customer(self, id_customer):

		users = self.db.session.query(User).filter(User.idreg_fk_customer == id_customer).all()

		return users

    def delete(self, uid: int):

		try:
			self.db.session.query(User).filter(User.id == uid).delete()
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()
			raise InvalidUserException