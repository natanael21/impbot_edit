from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from improve_bot.entities.perfil import Perfil

from improve_bot.repositories.i_perfiles_repository import IPerfilRepository

class PerfilRepositorySqlAlchemy(IPerfilRepository):

	@inject
	def __init__(self, db: SQLAlchemy):

		self.db = db

	def add(self, perfil: Perfil):

		try:
			self.db.session.add(perfil)
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()
			raise InvalidUserException

	def get_perfil_by_name(self, name_perfil: str):

		perfil = self.db.session.query(Perfil).filter(Perfil.name_perfil == name_perfil
													 ).first()
		if perfil:
			return perfil
		else:
			return None

	def get_perfil_by_id(self, id_perfil: int):

		perfil = self.db.session.query(Perfil).filter(Perfil.id == id_perfil).first()
		if perfil:
			return perfil
		raise InvalidPerfilException

	def get_all(self):

		return self.db.session.query(Perfil).filter(Perfil.id > 0).all()

	def update(self):

		self.db.session.commit()

	def delete(self, uid: int):

		try:
			self.db.session.query(Perfil).filter(Perfil.id == uid).delete()
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()
			raise InvalidUserException
