from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from sqlalchemy import cast, Date

from improve_bot.entities.application import Application
from improve_bot.repositories.i_application_repository import IApplicationRepository


class ApplicationRepostorySqlAlchemy(IApplicationRepository):
    