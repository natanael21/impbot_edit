from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from improve_bot.entities.sessions import Session

from improve_bot.repositories.i_sessions_repository import ISessionRepository

class SessionRepositorySqlAlchemy(ISessionRepository):

	@inject
	def __init__(self, db: SQLAlchemy):

		self.db = db

	def add(self, session: Session):

		try:
			self.db.session.add(session)
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()
			raise InvalidUserException

	def get_all(self):

		return self.db.session.query(Session).all()

	def get_all_by_user(self, name):

		return self.db.session.query(Session).filter(Session.user_name == name).all()

	def get_session(self, usuario: str, user_agent:str, ip: str):

		session = self.db.session.query(Session).filter(Session.user_name == usuario,
														Session.session_user_agent == user_agent,
														Session.session_ip_address == ip
														).order_by(Session.id.desc()).first()
		return session 

	def get(self, id_session:int):

		session = self.db.session.query(Session).filter(Session.id == id_session).first()
		return session

	def get_one_by_user(self, user_name:str):

		return self.db.session.query(Session).filter(Session.user_name == user_name
													).order_by(Session.id.desc()).first()


	def get_cant_by_user(self, usuario:str):

			return self.db.session.query(Session).filter(Session.user_name == usuario).count()

	def update(self):

		self.db.session.commit()

	def delete(self, uid: int):

		try:
			self.db.session.query(Session).filter(Session.id == uid).delete()
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()
			raise InvalidUserException