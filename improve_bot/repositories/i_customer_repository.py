from abc import ABC, abstractmethod

from improve_bot.entities.customers import Customer

class ICustomerRepository(ABC):

	@abstractmethod
	def add(self, customer: Customer):

		pass

	@abstractmethod
	def get_all(self):

		pass

	@abstractmethod
	def get_customer_by_id(self, id: int):

		pass

	@abstractmethod
	def delete(self):

		pass