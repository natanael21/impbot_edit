from abc import ABC, abstractmethod

from improve_bot.entities.menu import Menu

class IMenuRepository(ABC):

	@abstractmethod
	def add(self, menu: Menu):

		pass

	@abstractmethod
	def get_all(self):

		pass