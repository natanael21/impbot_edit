from abc import ABC, abstractmethod

from improve_bot.entities.perfil import Perfil

class IPerfilRepository(ABC):

	@abstractmethod
	def add(self, perfil: Perfil):

		pass

	@abstractmethod
	def get_all(self):

		pass

	@abstractmethod
	def delete(self, uid: int):

		pass