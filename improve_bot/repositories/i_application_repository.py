from abc import ABC, abstractmethod

from improve_bot.entities.application import Application

class IApplicationRepository(ABC):

    @abstractmethod
	def add(self, perfil: Perfil):

		pass

	@abstractmethod
	def get_all(self):

		pass

	@abstractmethod
	def delete(self, uid: int):

		pass