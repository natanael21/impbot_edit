from injector import inject

from improve_bot.entities.menu import Menu

from improve_bot.use_cases.i_menu_use_case import IMenuUseCase

from improve_bot.repositories.sql_alchemy.menu_repository_sql_alchemy import MenuRepositorySqlAlchemy

class MenuUseCase(IMenuUseCase):

	@inject
	def __init__(self, menu_repo: MenuRepositorySqlAlchemy):

		self.menu_repo = menu_repo

	def add(self, menu: Menu):

		self.menu_repo.add(Menu)

	def get(self):

		return self.menu_repo.get_all()

	def get_all(self):

		self.menu_repo.get_all()

	def get_rrss(self):

		menu_izquierda = self.menu_repo.get_all()
		rrss = []
		for raiz in menu_izquierda:
			if raiz.raiz == raiz.id:
				pass
			else:
				rrss.append(raiz)
		return rrss

	def get_menu(self):

		menu_izquierda = self.menu_repo.get_all()
		menus = []
		for raiz in menu_izquierda:
			if raiz.raiz == raiz.id:
				menus.append(raiz) 
			else:
				pass
		return menus