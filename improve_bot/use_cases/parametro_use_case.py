import datetime

from injector import inject

from improve_bot.entities.parametros import Parametro

from improve_bot.use_cases.i_parametro_use_case import IParametroUseCase

from improve_bot.repositories.sql_alchemy.parametros_repository_sql_alchemy import ParametroRepositorySqlAlchemy
from improve_bot.repositories.sql_alchemy.users_repository_sql_alchemy import UserRepositorySqlAlchemy

class ParametroUseCase(IParametroUseCase):

	@inject
	def __init__(self, param_repo: ParametroRepositorySqlAlchemy,
				user_repo: UserRepositorySqlAlchemy):

		self.param_repo = param_repo
		self.user_repo = user_repo

	def add(self, oParam: Parametro):

		self.param_repo.add(oParam)

	def get_parametros(self):

		params = self.param_repo.get_all()

		return params

	def get_user(self, id:int):

		return self.user_repo.get_user_by_id(id)

	def get_param_by_id(self, id:int):

		return self.param_repo.get_parametro_by_id(id)

	def update(self):

		self.param_repo.update()

	def delete(self, id:int):

		self.param_repo.delete(id)