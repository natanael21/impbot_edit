import datetime

from injector import inject
from flask_bcrypt import generate_password_hash

from improve-bot.entities.users import User
from improve-bot.entities.passwords import Password

from improve-bot.use_cases.i_user_use_case import IUserUseCase

from improve-bot.repositories.sql_alchemy.users_repository_sql_alchemy import UserRepositorySqlAlchemy
from improve-bot.repositories.sql_alchemy.perfiles_repository_sql_alchemy import PerfilRepositorySqlAlchemy
from improve-bot.repositories.sql_alchemy.sessions_repository_sql_alchemy import SessionRepositorySqlAlchemy
from improve-bot.repositories.sql_alchemy.passwords_repository_sql_alchemy import PasswordRepositorySqlAlchemy
from improve-bot.repositories.sql_alchemy.customer_repository_sql_alchemy import CustomerRepositorySqlAlchemy

class UserUseCase(IUserUseCase):

	@inject
	def __init__(self, user_repo: UserRepositorySqlAlchemy, session_repo : SessionRepositorySqlAlchemy,
				pass_repo : PasswordRepositorySqlAlchemy, perfil_repo:PerfilRepositorySqlAlchemy,
				cus_repo: CustomerRepositorySqlAlchemy):

		self.user_repo = user_repo
		self.session_repo = session_repo
		self.pass_repo = pass_repo
		self.perfil_repo = perfil_repo
		self.cus_repo = cus_repo

	def add(self, user: User):

		self.user_repo.add(user)

	def agregar_bloqueo(self, id_usuario:int, contrasena:str, audit_log_ins:int):

		oPass = Password(core_security_pass_userid= id_usuario , core_security_pass_hist_1=' ',
						core_security_pass_hist_2=' ', core_security_pass_hist_3=' ',
						core_security_pass_last= contrasena , core_security_pass_op= 1 ,
						core_security_pass_block= 0,core_security_pass_date_block= None,
						core_security_pass_ind = 0, core_security_pass_load_type= None,
						audit_log_ins= 1 , audit_log_upd = 1)
		self.pass_repo.add(oPass)

	def get(self):

		return self.user_repo.get_all()

	def get_perfiles(self):

		return self.perfil_repo.get_all()

	def get_perfil(self, nombre_perfil:str):

		return self.perfil_repo.get_perfil_by_name(nombre_perfil)

	def get_perfil_by_id(self, id:str):

		return self.perfil_repo.get_perfil_by_id(id)

	def get_user(self, id:int):

		return self.user_repo.get_user_by_id(id)

	def get_keyword_by_user(self, id):


		user = self.user_repo.get_user_by_id(id)

		customer_id = user.idreg_fk_customer 

		nub = self.cus_repo.get_nub_by_customer(customer_id)

		if nub != None:
			keyword = self.cus_repo.get_keyword_by_nub(nub.id)

			return keyword
		else:
			return ''
			
	def get_data_by_user(self, id:int, max_age, min_age):

		#esto deberia leer los nubs del customer, pero trae solo el primero por cada cliente (oct 2019)

		user = self.user_repo.get_user_by_id(id)

		customer_id = user.idreg_fk_customer 

		nub = self.cus_repo.get_nub_by_customer(customer_id)

		#print(nub)

		if nub != None:

			dataset = self.cus_repo.get_dataset_by_nub(nub.id, max_age, min_age)

			data = [d.text for d in dataset]

			print('len data: '+str(len(data)))
			return data

		else:

			return []

	def get_keywords_by_user(self, user_id):

		#listar las keywords del cliente (empresa) para poder elegir al inciar el dashboard

		keywords = []

		user = self.user_repo.get_user_by_id(user_id)

		customer_id = user.idreg_fk_customer 

		nubs = self.cus_repo.get_nubs_by_customer(customer_id)
		nubs_bbdd = self.cus_repo.get_bbdd_nubs_by_customer(customer_id)

		for nub in nubs:

			keyword = self.cus_repo.get_keyword_by_nub(nub.id)

			keywords.append(keyword)

		for nub in nubs_bbdd:

			keyword = nub.ddbb_table + '_' + nub.ddbb_field

			keywords.append(keyword)

		return keywords

	def get_dataset_by_keyword(self, keyword,  max_age, min_age, max_tweets=None):

		#esto lee los nubs del customer

		if keyword != None:

			nubs_ids= self.cus_repo.get_multiple_nubs_by_keyword(keyword)


			data = []

			print('cant nubs same keyword : '+ str(len(nubs_ids)))

			for nub_id in nubs_ids:

				print('nub_id : '+ str(nub_id))

				#get_dataset_by_nub deberia tener limite (pending)
				dataset = self.cus_repo.get_dataset_by_nub(nub_id, max_age, min_age)
				
				#por ahora me basta que el alguno de los nub/dataset no esté vacío
				if len(dataset) > 0:

					if max_tweets != None:

						

						for d in dataset[:max_tweets]:

							list_dataset = {}

							list_dataset['text'] = d.text
							list_dataset['screen_name'] = d.screen_name
							list_dataset['profile_image_url'] = d.profile_image_url

							data.append(list_dataset)

					else:


						

						for d in dataset:

							list_dataset = {}

							list_dataset['text'] = d.text
							list_dataset['screen_name'] = d.screen_name
							list_dataset['profile_image_url'] = d.profile_image_url

							data.append(list_dataset)

					print('len data (by keyword): '+str(len(data)))

			return data

		else:

			return []


	def get_dataset_from_bbdd(self, keyword, customer_id):

		return self.user_repo.get_dataset_from_text(keyword, customer_id)

	def get_user_by_username(self, username:str):

		return self.user_repo.get_user_by_username(username)

	def get_users_by_customer_id(self, id_customer:int):

		return self.user_repo.get_users_by_customer(id_customer)

	def get_all(self):

		return self.user_repo.get_all()

	def get_all_by_customer(self, iCus: int):

		return self.user_repo.get_users_by_customer(iCus)

	def get_cus_all(self):
		return self.cus_repo.get_all()

	def get_cus_by_name(self, sName:str):
		return self.cus_repo.get_by_name(sName)

	def get_cus_by_userid(self, id: int):
		oUser = self.user_repo.get_user_by_id(id)
		return self.cus_repo.get_customer_by_id(oUser.idreg_fk_customer)

	def obtener_pass_antiguos(self, id_usuario):

		oPass = self.pass_repo.get_pass(id_usuario)
		return [oPass.core_security_pass_last, oPass.core_security_pass_hist_1,
				oPass.core_security_pass_hist_2, oPass.core_security_pass_hist_3]

	def update_user(self, id:int, nombreusuario:str, nombre:str,
					path_foto:str, correo:str, audit_log_upd: int, audit_fec_upd,
					id_perfil:int ):

		user = self.user_repo.get_user_by_id(id)

		user.user_name = nombreusuario
		user.name = nombre
		if path_foto != 'no change':
			user.photo = path_foto
		user.audit_log_upd = audit_log_upd
		user.audit_fec_upd = audit_fec_upd
		verificacion_1 = self.perfil_repo.get_perfil_by_name('Administrador')
		verificacion_2 = self.perfil_repo.get_perfil_by_name('Desarrollador')
		if user.idreg_fk_perfil == verificacion_1.id or user.idreg_fk_perfil == verificacion_2.id:
			user.idreg_fk_perfil = id_perfil

		self.user_repo.update()


	def update_about(self, id, nombre,perfil,customer, photo, user_mail,
					mood, mobile, home, office, about_me, country_region, city,
					comuna, website, gender, fecha, language, audit_id = 1):

		user = self.user_repo.get_user_by_id(id)

		user.name = nombre
		user.idreg_fk_perfil = perfil
		user.idreg_fk_customer = customer
		if photo != 'no cambiar':
			user.photo= '/static/img/registro/'+ photo
		user.user_mail = user_mail
		user.mood = mood
		user.mobile = mobile
		user.home = home
		user.office = office
		user.about_me = about_me
		user.country_region = country_region
		user.city = city
		user.comuna = comuna
		user.website = website
		user.gender = gender
		user.birthdate = datetime.datetime.strptime(fecha, '%Y-%m-%d'),
		user.language = language
		user.audit_log_upd = audit_id
		user.audit_fec_upd = datetime.datetime.now()

		self.user_repo.update()

	def update(self):

		self.user_repo.update()

	def actualizar(self, id_usuario:int, contrasena:str):

		oPass = self.pass_repo.get_pass(id_usuario)
		valores = [oPass.core_security_pass_last, oPass.core_security_pass_hist_1,
					oPass.core_security_pass_hist_2, oPass.core_security_pass_hist_3]

		oPass.core_security_pass_last = generate_password_hash(contrasena)
		oPass.core_security_pass_hist_1 = valores[0]
		oPass.core_security_pass_hist_2 = valores[1]
		oPass.core_security_pass_hist_2 = valores[2]

		oPass.audit_log_upd = id_usuario
		oPass.audit_fec_upd = datetime.datetime.now()

		self.pass_repo.update()

		oUser = self.user_repo.get_user_by_id(id_usuario)

		oUser.password = generate_password_hash(contrasena)


		self.user_repo.update()


	def delete_user_trace(self, id_usuario):

		oUser = self.user_repo.get_user_by_id(id_usuario)
		oSession = self.session_repo.get_one_by_user(oUser.user_name)
		oPass = self.pass_repo.get_pass(oUser.id)
		try:
			self.session_repo.delete(oSession.id)
			self.session_repo.update()
		except:
			pass
		self.user_repo.delete(oUser.id)
		self.user_repo.update()
		self.pass_repo.delete(oPass.id)