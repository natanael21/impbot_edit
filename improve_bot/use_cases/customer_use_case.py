from injector import inject

from improve_bot.entities.customers import Customer

from improve_bot.use_cases.i_customer_use_case import ICustomerUseCase

from improve_bot.repositories.sql_alchemy.customer_repository_sql_alchemy import CustomerRepositorySqlAlchemy
from improve_bot.repositories.sql_alchemy.users_repository_sql_alchemy import UserRepositorySqlAlchemy

class CustomerUseCase(ICustomerUseCase):

	@inject
	def __init__(self,  user_repo: UserRepositorySqlAlchemy,
						customer_repo: CustomerRepositorySqlAlchemy):

		self.user_repo = user_repo
		self.customer_repo = customer_repo

	def add(self, customer: Customer):
		self.customer_repo.add(customer)

	def get(self):
		return self.customer_repo.get_all()

	def get_user(self, id:int):
		return self.user_repo.get_user_by_id(id)

	def get_customer_by_id(self, id):
		return self.customer_repo.get_customer_by_id(id)


	def get_customers(self):
		return self.customer_repo.get_all_without_active()


	def update(self):
		self.customer_repo.update()

	def delete(self, id):
		self.customer_repo.delete(id)