from injector import inject

from improve_bot.entities.perfil import Perfil

from improve_bot.use_cases.i_perfiles_use_case import IPerfilUseCase

from improve_bot.repositories.sql_alchemy.perfiles_repository_sql_alchemy import PerfilRepositorySqlAlchemy
from improve_bot.repositories.sql_alchemy.users_repository_sql_alchemy import UserRepositorySqlAlchemy
from improve_bot.repositories.sql_alchemy#importar parametros_sql_alchemy


class PerfilUseCase(IPerfilUseCase):

	@inject
	def __init__(self, perfil_repo: PerfilRepositorySqlAlchemy,
				user_repo: UserRepositorySqlAlchemy,
				param_repo: ParametroRepositorySqlAlchemy):

		self.perfil_repo = perfil_repo
		self.user_repo = user_repo
		self.param_repo = param_repo

	def add(self, perfil: Perfil):

		self.perfil_repo.add(perfil)

	def get(self):

		return self.perfil_repo.get_all()

	def get_all(self):

		return self.perfil_repo.get_all()
	
	def get_user(self, id:int):

		return self.user_repo.get_user_by_id(id)

	def get_perfil_by_id(self, id):

		return self.perfil_repo.get_perfil_by_id(id)

	def get_perfil_by_name(self, name:str):

		return self.perfil_repo.get_perfil_by_name(name)

	def get_user_by_id(self, id:int):

		return self.user_repo.get_user_by_id(id)

	def get_param(self, name:str):

		return self.param_repo.get_parametro_by_name(name)

	def update(self):

		self.perfil_repo.update()

	def delete_perfil(self, id:int):

		self.perfil_repo.delete(id)