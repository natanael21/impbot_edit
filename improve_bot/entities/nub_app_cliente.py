import datetime

class Nub_App_Cliente:

    def __init__(self, idreg_fk_app: int, idreg_fk_cliente: int,
                active: str, audit_log_ins: int,audit_log_upd: int):

        self.idreg_fk_app       = idreg_fk_app
        self.idreg_fk_cliente   = idreg_fk_cliente
        self.active             = active
        self.audit_log_ins  = audit_log_ins
        self.audit_fec_ins  = self.date_now()
		self.audit_log_upd  = audit_log_upd
		self.audit_fec_upd  = self.date_now()

    def date_now(self):

		x = datetime.datetime.now()
		return x