import datetime

class Perfil:

	def __init__(self, name_perfil: str, desc_perfil: str,active: str, audit_log_ins: int,
				audit_log_upd: int):

		self.name_perfil = name_perfil
		self.desc_perfil = desc_perfil
		self.active = active
		self.audit_log_ins = audit_log_ins
		self.audit_fec_ins = self.date_now()
		self.audit_log_upd = audit_log_upd
		self.audit_fec_upd = self.date_now()

	def date_now(self):

		x = datetime.datetime.now()
		return x
