import datetime, time

class Nub_usu_can_api:

    def __init__(self, idreg_fk_usuario: int, idreg_fk_canal: int, idreg_fk_api:int,
                api_key: str, api_secret:str, api_name:str, periodo_dias:int,
                hora_programacion: time, fecha_termino: datetime,
                active: str, audit_log_ins: int, audit_log_upd: int):

        self.idreg_fk_usuario       = idreg_fk_usuario
        self.idreg_fk_canal         = idreg_fk_canal
        self.idreg_fk_api           = idreg_fk_api
        self.api_key                = api_key
        self.api_secret             = api_secret
        self.api_name               = api_name
        self.periodo_dias           = periodo_dias
        self.hora_programacion      = self.time_now()
        self.fecha_termino          = self.date_now()
        self.active                 = active
		self.audit_log_ins          = audit_log_ins
		self.audit_fec_ins          = self.date_now()
		self.audit_log_upd          = audit_log_upd
		self.audit_fec_upd          = self.date_now()

    def date_now(self):

		x = datetime.now()
		return x

    def time_now(self):
        x = datetime.datetime.now().time
        return x