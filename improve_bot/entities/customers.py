import datetime

class Customer:

	def __init__(self, idreg_fk_plan: int,rut_customer: str, name_customer: str,
				desc_customer: str, address_customer: str, phone_customer: str,
				rubro_customer: str, nro_workers:int, email_customer: str, logo_customer:str,
				active: str, audit_log_ins: int, audit_log_upd: int):

		self.idreg_fk_plan    = idreg_fk_plan 
        self.rut_customer     = rut_customer
		self.name_customer    = name_customer
		self.desc_customer    = desc_customer
		self.address_customer = address_customer
		self.phone_customer   = phone_customer
		self.rubro_customer   = rubro_customer
		self.nro_workers      = nro_workers
		self.email_customer   = email_customer
		self.logo_customer    = logo_customer
		self.active           = active
		self.audit_log_ins    = audit_log_ins 
		self.audit_fec_ins    = self.date_now()
		self.audit_log_upd    = audit_log_upd
		self.audit_fec_upd    = self.date_now()

	def date_now(self):

		x = datetime.datetime.now()
		return x