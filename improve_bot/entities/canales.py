import datetime

class Canales:

    def __init__(self, nombre_canal: str, desc_canal: str,
                active: str, audit_log_ins: int, audit_log_upd: int):

        self.nombre_canal       = nombre_canal
        self.desc_canal         = desc_canal
        self.active             = active
		    self.audit_log_ins      = audit_log_ins 
		    self.audit_fec_ins      = self.date_now()
		    self.audit_log_upd      = audit_log_upd
		    self.audit_fec_upd      = self.date_now()

    def date_now(self):
      x = datetime.datetime.now()
		return x