from datetime import datetime

class Menu:

	def __init__(self,name_menu: str, raiz: str, link: str,
				active: str, audit_log_ins: int, audit_log_upd: int):

		self.name_menu = name_menu
		self.raiz = raiz
		self.link = link
		self.active = active
		self.audit_log_ins = audit_log_ins
		self.audit_fec_ins = self.date_now()
		self.audit_log_upd = audit_log_upd
		self.audit_fec_upd = self.date_now()

	def date_now(self):

		x = datetime.now()
		return x