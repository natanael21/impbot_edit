import datetime

class Session:

    def __init__(self, user_name: str, session_ip_address: str, session_remote_host: str, 
				session_user_agent: str, session_start_time: datetime, session_end_time: datetime,
				session_time: int, session_show_popup: int, active: str, audit_fec_ins: datetime):

		self.user_name = user_name
		self.session_ip_address = session_ip_address
		self.session_remote_host = session_remote_host
		self.session_user_agent = session_user_agent
		self.session_start_time = session_start_time
		self.session_end_time = session_end_time
		self.session_time = session_time
		self.session_show_popup = session_show_popup
		self.active = active
		self.audit_fec_ins = audit_fec_ins
        