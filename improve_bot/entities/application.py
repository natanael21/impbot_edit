import datetime

class Application:

    def __init__(self, name_app: str, desc_app: str, url_app:str,
                icon_app: str, active: str, audit_log_ins: int,audit_log_upd: int ):

        self.name_app       = name_app
        self.desc_app       = desc_app
        self.url_app        = url_app
        self.icon_app       = icon_app
        self.active         = active
        self.audit_log_ins  = audit_log_ins
        self.audit_fec_ins  = self.date_now()
		    self.audit_log_upd  = audit_log_upd
		    self.audit_fec_upd  = self.date_now()

    def date_now(self):

		x = datetime.datetime.now()
		return x