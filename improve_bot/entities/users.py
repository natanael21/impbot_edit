import datetime

from flask_bcrypt import generate_password_hash

class User:
    def __init__(self, idreg_fk_perfil=1, idreg_fk_customer=1, user_name='', name='',
                mood='', mobile='',home='', office='', about_me='' , country_region='',
	            city='', comuna='', website='', gender='', birthdate=datetime.datetime.now(),
	            password='', language='', password_v2='', first_time='n',tutorial='', account_sid='',
                auth_token='', ghost_user='', photo='', licencia='',
	            user_mail='', user_mail_gmail='', consentimiento='n',
	            fecha_con=datetime.datetime.now(), active='n',
	            audit_fec_first_time=datetime.datetime.now(), audit_log_ins=1,
	            audit_log_upd=1):

        self.idreg_fk_perfil = idreg_fk_perfil
        self.idreg_fk_customer = idreg_fk_customer
		self.user_name = user_name
		self.name = name
		self.mood = mood
		self.mobile = mobile
		self.home = home
		self.office = office
		self.about_me = about_me
		self.country_region = country_region
		self.city = city
		self.comuna = comuna
		self.website = website
		self.gender = gender
		self.birthdate = birthdate
		self.password = generate_password_hash(password)
		self.language = language
		self.password_v2 = password_v2
		self.first_time = first_time
		self.tutorial = tutorial
		self.account_sid = account_sid
		self.auth_token = auth_token
		self.ghost_user = ghost_user
		self.photo = photo
		self.licencia = licencia
		self.user_mail = user_mail
		self.user_mail_gmail = user_mail_gmail
		self.consentimiento = consentimiento
		self.fecha_con = fecha_con
		self.active = active
		self.audit_fec_first_time = audit_fec_first_time
		self.audit_log_ins = audit_log_ins
		self.audit_fec_ins = self.date_now()
		self.audit_log_upd = audit_log_upd
		self.audit_fec_upd = self.date_now()

    def date_now(self):

		x = datetime.datetime.now()
		return x