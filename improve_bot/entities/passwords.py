import datetime

from flask_bcrypt import generate_password_hash

class Password:

	def __init__(self, core_security_pass_userid: str, core_security_pass_hist_1: str, 
				core_security_pass_hist_2: str, core_security_pass_hist_3: str,
				core_security_pass_last: str, core_security_pass_op: int, core_security_pass_block: int,
				core_security_pass_date_block: datetime, core_security_pass_ind : int,core_security_pass_load_type: str,
				audit_log_ins: int, audit_log_upd: int):

		self.core_security_pass_userid = core_security_pass_userid
		self.core_security_pass_hist_1 = generate_password_hash(core_security_pass_hist_1)
		self.core_security_pass_hist_2 = generate_password_hash(core_security_pass_hist_2)
		self.core_security_pass_hist_3 = generate_password_hash(core_security_pass_hist_3)
		self.core_security_pass_last = generate_password_hash(core_security_pass_last)
		self.core_security_pass_op = core_security_pass_op
		self.core_security_pass_block = core_security_pass_block
		self.core_security_pass_date_block = core_security_pass_date_block
		self.core_security_pass_load_type = core_security_pass_load_type
		self.core_security_pass_ind = core_security_pass_ind
		self.audit_log_ins = audit_log_ins
		self.audit_fec_ins = datetime.datetime.now()
		self.audit_log_upd = audit_log_upd
		self.audit_fec_upd = datetime.datetime.now()